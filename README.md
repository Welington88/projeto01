![logo@2x](https://user-images.githubusercontent.com/51893051/132037475-729d3f54-19bf-46f2-a01d-c8e5fb35b7f7.png)
### **Principais** Comandos Git

- git init -> Inicia uma Repositório Git
- git status -> Status atual do Repositório Git
- git add [NomeArquivo] -> Adicionar o Arquivo para Controle de Versão
- git add . -> Adicionar todos os arquivos ao Controle de Versão
- git remote add origin [NomeRepositorio] -> Config repositório Remoto
- git commit -m "Descrição do Commit" -> fazer o commit
- git push -u origin master -> enviar para repositório remoto
- git pull --rebase origin -> puxa as alterações para máquina local
- git log -p -2 -> para ver o status
- git tag 0.1.0 -> cria uma tag
- git tag -l -> traz todos as tags
- git push origin --tags -> faz uploads da tag
- git clone -> traz repositório para máquina local
- git reset --hard HEAD~1   -> desfazer o último commit apaga tudo
- git reset --soft HEAD~1   -> desfazer o último commit não paga
- git merge [NomeBranch] -> faz merge com branch selecionada 
- git reset -–merge -> desfazer o merge 
- git branch -D nome-da-branch -> Apaga a branch
- git branch --list -> Lista de branch
- vim .gitignore  -> Para ignorar arquivos
- git config --global user.name “Seu Nome“
- git config --global user.email "Seu Email"
- git config --get-all user.name
- git config --get-all user.email
- git checkout -b MinhaNovaBrach -> cria Nova Branch
- git checkout MinhaNovaBrach -> Acessa a Branch
- 
### **Chave** SSH 
- Chave ssh-keygen -> Gerar Chave ssh
- Cat id_rsa.pub -> imprimi a chave ssh na pasta que foi gerada normalmente pasta padrão do pc
